import React, {useState, useEffect} from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import {
    Table,TableCell, TableBody, CircularProgress,TableContainer, TableHead, TableRow, Typography, Paper, LinearProgress
} from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';

const StyledTableCell = withStyles((theme) => ({
  head: {
    // backgroundColor: theme.palette.common.black,
    backgroundColor: '#F9FAFB',
    // color: theme.palette.common.white,
    color: 'black',
    borderTopColor: '#3F51B5',
    borderTopWidth: '5px',
    borderTopStyle: 'solid'
  },
  body: {
    fontSize: 14,
  }
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
    //   backgroundColor: theme.palette.action.hover,
      backgroundColor: '#D6E4F2',
    },
  },
  hover: {
    backgroundColor: 'yellow'
  }
}))(TableRow);


const LoadingList = () => {
    return (
        <StyledTableRow style={{width: 1200, backgroundColor: '#2980B92E', height: 500}}>
            <StyledTableCell  colSpan='8' align="center" style={{width: '700px'}}>
                <Typography variant="h1">
                    <CircularProgress />
                </Typography>
            </StyledTableCell>
        </StyledTableRow>
    )
}

const NotFound = () => {
    return (
        <StyledTableRow style={{width: 1200, backgroundColor: '#2980B92E', height: 500}}>
            <StyledTableCell  colSpan='8' align="center" style={{width: '700px'}}>
                <Typography variant="h1">
                    <h1>Data Not Found</h1>
                </Typography>
            </StyledTableCell>
        </StyledTableRow>
    )
}





const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
  read: {
    fontSize: 12,
    
  },
  unread: {
    fontSize: 13,
    fontWeight: 'bold'
  },
  tablerow: {
    backgroundColor: '#ECF0F4'
  }
});

const ListPenting = () => {
    return (
        <h1>Pesan penting</h1>
    )
}

export default ListPenting;
