import React, {useState, useEffect} from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import {
    Table,TableCell, TableBody, CircularProgress,TableContainer, 
    TableHead, TableRow, Typography, Paper, LinearProgress, Link,
    IconButton, Grid, TextareaAutosize, TextField, Button
} from '@material-ui/core';
import Chip from '@material-ui/core/Chip';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Pagination from '@material-ui/lab/Pagination';
import Skeleton from '@material-ui/lab/Skeleton';
import TogglePenting from './TogglePenting';
// import good1 from '../assets/gif/good1.gif';
// import good2 from '../assets/gif/good2.gif';
// import notfound from '../assets/gif/notfound.gif';
import notfound2 from '../assets/img/page-not-found.png'
import searchgif from '../assets/gif/Search.gif';
import ToggleDelete from './ToggleDelete';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import StarIcon from '@material-ui/icons/Star';
import SaveIcon from '@material-ui/icons/Save';
import { 
    get_ingatkan_all, 
    deleteingatkan ,
    URLSuratbyNid } from '../constants';
import moment from 'moment';

const StyledTableCell = withStyles((theme) => ({
  head: {
    // backgroundColor: theme.palette.common.black,
    backgroundColor: '#F9FAFB',
    // color: theme.palette.common.white,
    color: 'black',
    borderTopColor: '#3F51B5',
    borderTopWidth: '5px',
    borderTopStyle: 'solid'
  },
  body: {
    fontSize: 14,
  }
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
    //   backgroundColor: theme.palette.action.hover,
      backgroundColor: '#D6E4F2',
    },
  },
  hover: {
    backgroundColor: 'yellow'
  }
}))(TableRow);




function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 700,
    },
    read: {
        fontSize: 12,

    },
    unread: {
        fontSize: 13,
        fontWeight: 'bold'
    },
    tablerow: {
        backgroundColor: '#ECF0F4'
    },
    root: {
        width: '100%',
        '& > * + *': {
          marginTop: '1em',
        },
    },
    button: {
        margin: theme.spacing(1),
    },
}));

const LoadingList = () => {
    return (
        <StyledTableRow style={{width: 1200, backgroundColor: '#DEE3F1', height: 500}}>
            <StyledTableCell  colSpan='8' align="center" style={{width: '700px'}}>
                <Typography variant="h1">
                    <CircularProgress />
                    {/* <img src={searchgif} style={{height: 'auto', width: '60%'}}/> */}
                    {/* <Typography>Searching data what you want </Typography> */}
                </Typography>
            </StyledTableCell>
        </StyledTableRow>
    )
}

const NotFound = () => {
    return (
        <StyledTableRow style={{width: 1200, backgroundColor: '#F0F4F8', height: 500}}>
            <StyledTableCell  colSpan='8' align="center" style={{width: '700px'}}>
                <Typography variant="h1">
                    <img src={notfound2} style={{height: 'auto', width: '30%'}}/>
                </Typography>
                <Typography variant="subtitle1" style={{color: '#617E96'}}> Maaf, surat tidak ditemukan</Typography>
            </StyledTableCell>
        </StyledTableRow>
    )
}


const Penting  = (props) => {
    const classes = useStyles();
    const [data,setData]            = useState(null);
    const [loading,setLoading]      = useState(true)
    const [peopleId, setPeopleId]   = useState(null)
    const [primaryRoleId, setPrimaryRoleId] = useState(null)
    const [notFound, setNotFound] = useState(false)
    const [totalPage, setTotalPage] = useState(0)
    const [page, setPage]           = useState(1)
    const [state, setState] = useState({
        success: false,
        error: false,
        vertical: 'center',
        horizontal: 'bottom',
        message: ''
    });
    const { message, vertical, horizontal, success, error } = state;
    
    const body = {
        people_id: 592,
        role_id: "uk.1.1.18.26",
        page: page,
        page_size: 10
    }

    const fetchProps = async() => {
        try{
            const fetchPenting = await fetch(get_ingatkan_all, {
                method: 'POST',
                mode: 'cors',
                headers: {"Content-type": "application/json"},
                body: JSON.stringify(body)
            })
            const resPenting = await fetchPenting.json()
            
            setData(resPenting.result)
            setTotalPage(resPenting.totalPages)
            setNotFound(resPenting.result == null ? true : false)
            setLoading(false)
            

        } catch (e){
            console.log('error penting', e)
        }
    }

    useEffect(() => {
        setLoading(true)
        // const peopleID = props.peopleId;
        // const roleeee = props.primaryRoleId;
        const peopleID = 592;
        const roleeee = 'uk.1.1.18.26';
        
        
        fetchProps()
    },[page])

    const handleChangeDelete = async(NId) => {
        const peopleID = 592;
        const roleeee = 'uk.1.1.18.26';
        console.log(props)
        try{
            const fetchDelete = await fetch(deleteingatkan + NId ,{
                method: 'POST',
                mode: 'cors',
                body: null
            })
            const resDelete = await fetchDelete.json()
            console.log('resDelete', resDelete)

            fetchProps()
            setState({
                success: true,
                message: 'Daftar ingatkan berhasil dihapus ...'
            })
           
           
       } catch {
        setNotFound(true)
        setData(data)
        setState({
            error: true,
            vertical: 'center',
            horizontal: 'bottom',
            message: 'Gagal menghapus ingatkan ...'
        })
       }
    }
    const handlePage = (event, value) => {
        setPage(value);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setState({
            success: false,
            error: false
        })
      };
    
    
    return (
        <>
        <TableContainer component={Paper}>
            {/* <LinearProgress style={{marginBottom: '-.4em'}} /> */}
            
            <Table className={classes.table} aria-label="customized table">
                {loading == true ? '' : notFound == true ? '' : 
                 <TableHead>
                 <TableRow>
                    <StyledTableCell align="center"  style={{}}>Ingatkan</StyledTableCell>
                    <StyledTableCell align="left"  style={{}}>Status Naskah</StyledTableCell>
                    <StyledTableCell align="left"  style={{}}>Asal Naskah</StyledTableCell>
                    <StyledTableCell align="left"  style={{}}>Pengirim</StyledTableCell>
                    <StyledTableCell align="left"  style={{}}>Hal</StyledTableCell>
                    <StyledTableCell align="left"  style={{}}>Tanggal</StyledTableCell>
                    <StyledTableCell align="left"  style={{}}>Catatan</StyledTableCell>
                    {/* <StyledTableCell align="left"  style={{}}></StyledTableCell> */}

               
                 </TableRow>
                 </TableHead>
                 }
               
                <TableBody>
                   
                {loading == true ? <LoadingList/> : notFound == true ? <NotFound/> : data&&data.map((row, key) => (
                     <StyledTableRow key={key} hover={true} className={classes.tablerow} >
                        <StyledTableCell className={row.StatusReceive == 'unread' ? classes.unread : classes.read} align="center" style={{width: '4%', fontWeight: 'bold', fontSize: '12px'}}> 
                            
                            <div>
                                <IconButton >
                                    {notFound 
                                        ? <StarIcon onClick={() => handleChangeDelete(row && row.ingatkan_id)} style={{color: '#FFC107', fontSize: '1.5em'}}  /> 
                                        : <StarIcon onClick={() => handleChangeDelete(row && row.ingatkan_id)} style={{color: '#FFC107', fontSize: '1.5em'}} />
                                    }
                                </IconButton>
                            </div>
                        </StyledTableCell>
                        <StyledTableCell className={row && row.data && row.data.StatusReceive  == 'unread' ? classes.unread : classes.read} align="left" style={{width: '5%'}}> 
                            <Link href={`${URLSuratbyNid}${row && row.data && row.data.NId }`} className={row && row.data && row.data.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit">
                                    <Typography variant='caption' style={{fontWeight: 'bold', fontSize: 12}}>Jenis Surat :</Typography>
                                    <Chip size="small" label={row && row.data && row.data.jenis_naskah} style={{backgroundColor: 'amber'}} color={row && row.data && row.data.jenis_naskah === 'Disposisi' ? 'Secondary' : row && row.data && row.data.jenis_naskah === 'Nota Dinas' ? 'Primary' : ''}/>
                                    <div style={{paddingTop: '.3em'}}><Typography variant='caption' style={{fontWeight: 'bold', fontSize: 12, paddingTop: '1em'}}>Baca :</Typography></div>
                                    <div><Chip size="small" label={row && row.data && row.data.StatusReceive == 'unread' ? 'Belum Dibaca' : 'Sudah Dibaca'} style={{backgroundColor: row && row.data && row.data.StatusReceive == 'unread' ? 'rgb(255, 193, 7)' : '' }} /></div>
                                    <div style={{paddingTop: '.3em'}}><Typography variant='caption' style={{fontWeight: 'bold', fontSize: 12, paddingTop: '1em'}}>Response Time :</Typography></div>
                                    <Chip size="small" label={row && row.data && row.data.StatusReceive == 'unread' ? row && row.data && row.data.differences_text : row && row.data && row.data.differences_text} style={{backgroundColor: row && row.data && row.data.StatusReceive == 'unread' ? 'rgb(255, 193, 7)' : '' }} />
                                    
                                
                            </Link>
                        </StyledTableCell>
                        <StyledTableCell className={row && row.data && row.data.StatusReceive  == 'unread' ? classes.unread : classes.read} align="left" style={{width: '9%'}}>
                            <Link href={`${URLSuratbyNid}${row && row.data && row.data.NId}`} className={row && row.data && row.data.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit">
                                <Typography variant='h2' style={{fontSize: 12, fontWeight: row && row.data && row.data.StatusReceive === 'unread' ? 'bold' : ''}}>{row && row.data && row.data.asal_naskah}</Typography>
                            </Link>
                        </StyledTableCell>
                        <StyledTableCell className={row && row.data && row.data.StatusReceive  == 'unread' ? classes.unread : classes.read} align="left" style={{width: '15%'}}>
                            <Link href={`${URLSuratbyNid}${row && row.data && row.data.NId}`} className={row && row.data && row.data.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit">
                                <Typography variant='caption' style={{fontSize: 12, fontWeight: row && row.data && row.data.StatusReceive === 'unread' ? 'bold' : ''}}>{row && row.data && row.data.nama_pengirim_sql}</Typography>
                                <Typography variant='h2' style={{fontSize: 12, fontWeight: row && row.data && row.data.StatusReceive === 'unread' ? 'bold' : ''}}>{row && row.data && row.data.jabatan_pengirim_sql}</Typography>
                            </Link>  
                        </StyledTableCell>
                        <StyledTableCell className={row && row.data && row.data.StatusReceive  == 'unread' ? classes.unread : classes.read} align="left" style={{width: '33%'}}>
                            <Link href={`${URLSuratbyNid}${row && row.data && row.data.NId}`} className={row && row.data && row.data.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit">
                                
                                <Typography variant='caption' style={{fontWeight: 'bold', fontSize: 12}}>Nomor Naskah :</Typography>
                                <Typography variant='h2' style={{fontSize: 12}}>{row && row.data && row.data.Nomor}</Typography>
                                <div style={{paddingTop: '1em'}}>
                                    <Typography variant='caption' style={{fontWeight: 'bold', fontSize: 12}}>Hal :</Typography>
                                    <Typography variant='h2' style={{fontSize: 12}}>{row && row.data && row.data.Hal}</Typography>
                                </div>
                            </Link>
                        
                        </StyledTableCell>
{/*                         
                        <StyledTableCell className={row && row.data && row.data.StatusReceive  == 'unread' ? classes.unread : classes.read} align="left" style={{width: '8%'}}>
                            <Link href={`${URLSuratbyNid}${row.NId}`} className={row && row.data && row.data.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit">{row && row.data && moment(row.data.ReceiveDate).format('YYYY-MM-DD HH:mm:ss')}</Link>
                            
                        </StyledTableCell> */}
                        <StyledTableCell className={row && row.data && row.data.StatusReceive  == 'unread' ? classes.unread : classes.read} align="left" style={{width: '9%', textAlign: 'left'}}>
                            <Typography variant='caption' style={{fontWeight: 'bold', fontSize: 12}}>Tanggal Surat Diterima:</Typography>
                            <div><Typography variant='h2' style={{fontSize: 12}}>{moment(row && row.data && row.data.ReceiveDate).format('YYYY-MM-DD HH:mm:ss')}</Typography></div>
                            {/* <div>
                            <Link href={`${URLSuratbyNid}${row.NId}`} className={row && row.data && row.data.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit" >
                                <Chip color="primary" label={moment(row && row.data && row.data.ReceiveDate).format('YYYY-MM-DD HH:mm:ss')} />
                            </Link>
                            </div> */}
                            <div style={{paddingTop: '1em'}}><Typography variant='caption' style={{fontWeight: 'bold', fontSize: 12, paddingTop: .7}}>Tanggal Ingatkan / Catatan:</Typography></div>
                            <div><Typography variant='h2' style={{fontSize: 12}}>{row && row.last_update}</Typography></div>
                            <div style={{paddingTop: '.5em'}}>
                            <Link href={`${URLSuratbyNid}${row.NId}`} className={row && row.data && row.data.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit" >
                                <Chip color="primary" label={row && row.last_update2} />
                            </Link>
                            </div>
                            
                            
                            
                        </StyledTableCell>
                        <StyledTableCell className={row && row.data && row.data.StatusReceive  == 'unread' ? classes.unread : classes.read} align="left" style={{width: '33%'}}>
                        
                                <TextField
                                    id="outlined-multiline-static"
                                    label="Catatan"
                                    multiline
                                    rows={4}
                                    defaultValue={row && row.note}
                                    variant="outlined"
                                    inputProps={{
                                        style: {fontSize: 12} 
                                      }}
                                      disabled
                                    style={{width: '200px', backgroundColor: 'white', fontSize: '11px'}}
                                />
                                <Button
                                    variant="contained"
                                    color="primary"
                                    size="small"
                                    className={classes.button}
                                    startIcon={<SaveIcon />}
                                    disabled
                                    style={{marginLeft: '4em'}}
                                >
                                    Save
                                </Button>
                            <Link href={`${URLSuratbyNid}${row.NId}`} className={row && row.data && row.data.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit">
                            
                                </Link>
                        </StyledTableCell>
                        {/* <StyledTableCell className={row.StatusReceive == 'unread' ? classes.unread : classes.read} align="left" style={{width: '6%'}}>
                            <Link href={`https://sikd.mkri.id/index3.php?option=MailTL&id=${row.NId}`} className={row && row.data && row.data.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit">{row.ds}</Link>
                            
                        </StyledTableCell> */}
                    </StyledTableRow>
                    
                ))}
              
                </TableBody>
            </Table>
        </TableContainer>

        {notFound || loading == true 
            ? '' 
            : 
            <div className={classes.pagination}>
                <Grid container direction="row" justify="center"  alignItems="center">
                    <Grid item>
                        {/* <Pagination count={30} color="primary" /> */}
                        <Pagination count={totalPage > 1 ? totalPage : 1 } page={totalPage > 1 ? page : 1} onChange={handlePage} color="primary"/>
                    </Grid>
                </Grid>
            </div>
        }

        <div className={classes.root}>
            <Snackbar 
                open={success} 
                autoHideDuration={4000} 
                onClose={handleClose}
                // anchorOrigin={{ vertical, horizontal }}
                // key={vertical + horizontal}
            >
                <Alert onClose={handleClose} severity="success">
                {message}
                </Alert>
            </Snackbar>
            <Snackbar 
                open={error} 
                autoHideDuration={4000} 
                onClose={handleClose}
                // anchorOrigin={{ vertical, horizontal }}
                // key={vertical + horizontal}
            >
                <Alert onClose={handleClose} severity="error">
                {message}
                </Alert>
            </Snackbar>
    
        </div>
        </>
    )
}

export default Penting;