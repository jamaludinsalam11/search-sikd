import React from 'react';


const SkeletonWaiting = () => {
    return (
        <>
        <div class="ui segment fluid">
            <div class="ui active inverted dimmer">
                <div class="ui large text loader">Loading</div>
            </div>
            <p></p>
            <p></p>
            <p></p>
        </div>
       
         
        </>
    )
}

export default SkeletonWaiting;