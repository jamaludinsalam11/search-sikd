import React, {useState, useEffect} from 'react';
import {
    IconButton
} from '@material-ui/core';
import TurnedInNotIcon from '@material-ui/icons/TurnedInNot';
import TurnedInIcon from '@material-ui/icons/TurnedIn';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import StarIcon from '@material-ui/icons/Star';
import moment from 'moment';

const ToggleDelete = (props) => {
    const [found, setFound] = useState(false)
    const [pentingId, setPentingId] = useState(null)
    const [peopleId, setPeopleId] = useState(props.peopleId)
    const [primaryRoleId, setPrimaryRoleId] = useState(props.primaryRoleId)
    const [data, setData] = useState(null);
    // console.log(data)
    useEffect(()=> {
        const dataTemp = props.data;
        const jams = props;
        // console.log('dataTemp', dataTemp)
        // console.log('propsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss',jams)
        const fetchData = async() => {
            // const peopleid = 576;
            // const primaryroleid = 'uk.1.1';
            const peopleID = dataTemp.To_Id;
            const roleeee = dataTemp.RoleId_To;
            const nid = dataTemp.NId;
            const penting_id = `${nid}-${roleeee}`;
            // console.log(nid)
            try {
                const api = `https://sikd_jamal.mkri.id/php-rest-api2/api/mailinbox/getId_penting.php?penting_id=${penting_id}`;
                // console.log('API, TOGGLE PENTING',dataTemp)
                await fetch(api)
                    .then((response) => {
                        return response.json();
                    })
                    .then((result) => {
                        // console.log('result',result)
                        setFound(result.status === 'success' ? true : false)
                        setPeopleId(peopleID)
                        setPrimaryRoleId(roleeee)
                        setData(props.data)
                        // setPentingId(result.data)
                    })

            } catch {

            }
        }
        fetchData();
    }, [found])

    function refreshPage() {

		window.location.reload(false);

	}

    const handleChangeInsert = (e) => {
        const date = moment(new Date()).format('YYYY-MM-DD h:mm:ss')
        const data2  = {
            // toid: data.To_Id,
            // roleidto: data.RoleId_To,
            nid: data.NId,
            peopleid: data.To_Id,
            primaryroleid: data.RoleId_To,
            date: moment(new Date()).format('YYYY-MM-DD h:mm:ss'),
            hal: data.Hal,
            instansipengirim: data.InstansiPengirim,
            jabatanpengirim: data.JabatanPengirim,
            nomor: data.Nomor,
            pengirim: data.Pengirim,
            receivedate: data.ReceiveDate,
            statusreceive: data.StatusReceive,
            tgl: data.Tgl,
            ds: data.ds,
            zz: data.zz
        }
        
       try{
        console.log('Success:', data2);
            fetch(`https://sikd_jamal.mkri.id/php-rest-api2/api/mailinbox/create_penting.php`, {
                method: 'POST',
                // mode: 'no-cors', 
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data2),
            })
            .then(response => response.json())
            .then(data => {
                // console.log('Success:', data2);
                setFound(true)
              })
              .catch((error) => {
                // console.error('Error:', error);
                setFound(false)
              });
              
       } catch {
        setFound(false)
       }
    }
   
    const handleChangeDelete = async() => {
        try{
            // console.log('tenangjamaldisini',data2)
            // const api = 'https://sikd_jamal.mkri.id/php-rest-api2/api/mailinbox/delete_penting.php';
            const api = `http://localhost:3001/sikd/api/v1/search-mailinbox/ingatkan/delete/${data.penting_id}`;

            const fetchDelete = await fetch(api ,{
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: null
            })
            const resDelete = await fetchDelete.json()
            console.log('resDelete', resDelete)
           
       } catch {
        setFound(true)
       }
    }

    // console.log("PEOPLEID FROM TOGGLE PENTING JS", peopleId)
    // console.log("PEOPLEID FROM TOGGLE PENTING JS", peopleId)
    // console.log("PRIMARYROLEID FROM TOGGLE PENTING JS", primaryRoleId)
    // console.log('API, TOGGLE PENTING',data)
    // console.log('FOUND',found)
    return (
        <div>
            <IconButton >
                {found 
                    ? <StarIcon onClick={handleChangeDelete} style={{color: '#FFC107', fontSize: '1.5em'}} /> 
                    : <StarBorderIcon onClick={handleChangeDelete} style={{ color: '#FFC107', fontSize: '1.5em'}}/>
                }
            </IconButton>
        </div>
        
        
    )
}

export default ToggleDelete;