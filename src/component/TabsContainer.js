import React from 'react';
import { Tabs, Tab, AppBar } from "@material-ui/core";
import { Route, BrowserRouter, Switch, Link } from "react-router-dom";

import ListPesan from './ListPesan';
import ListPenting from './ListPenting'

const TabsContainer = () => {
    const routes = ["/semua", "/penting"]
    return (
        <div>
            <BrowserRouter>
                <Route
                    path="/"
                    render={(history) => (
                        <Tabs 
                            value={
                                history.location.pathname !== "/"
                                    ? history.location.pathname
                                    : false
                            }
                        >
                            <Tab 
                                value={routes[0]}
                                label="penting"
                                component={Link}
                                to={routes[0]}
                            />
                            <Tab 
                                value={routes[0]}
                                label="semua"
                                component={Link}
                                to={routes[1]}
                            />
                            

                        </Tabs>
                    )}
                />

                <Switch>
                    <Route path="/semua" component={ListPesan} />
                    <Route path="/penting" component={ListPenting} />
                </Switch>

                
            </BrowserRouter>



             
        </div>
    )
}

export default TabsContainer;