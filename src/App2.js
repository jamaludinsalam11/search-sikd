import React,{useState,forwardRef, useEffect} from 'react';
import {Container, TextField, Tabs, Tab, Grid, Paper, IconButton,Fab, Divider,Typography, 
    TableContainer, Table, TableHead, TableRow, Chip, TableBody, TableCell, InputBase, FormControl, InputLabel} from '@material-ui/core'

const App2 = () => {
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);
    const [totalItems, setTotalItems] = useState(null);
    const [q, setQ] = useState("");
    const [searchParam] = useState(["Hal", "Nomor", "NId"]);
    const [filterParam, setFilterParam] = useState(["ternal"]);


    useEffect(() => {
        fetch(`https://sikd_jamal.mkri.id/php-rest-api2/api/mailinbox/inbox_api.php?begin=1&row=100&primaryroleid=uk.1.1&sumber=ternal&read=read&tgl1="2021-06-06 17:52:52"&tgl2="2021-06-16 17:52:52"&unit=&sort=DESC&search=hal&q=`)
            .then((res) =>{ 
                return res.json()
            })
            .then(
                (result) => {
                    console.log('result', result.data);
                    setTotalItems(result.total_data)
                    // setIsLoaded(true);
                    setItems(result.data);
                },
                (error) => {
                    console.log(error)
                }
            );
    }, []);

    function search(items) {
        return items.filter((item) => {
            if (item.region == filterParam) {
                return searchParam.some((newItem) => {
                    return (
                        item[newItem]
                            .toString()
                            .toLowerCase()
                            .indexOf(q.toLowerCase()) > -1
                    );
                });
            } else if (filterParam == "ternal") {
                return searchParam.some((newItem) => {
                    return (
                        item[newItem]
                            .toString()
                            .toLowerCase()
                            .indexOf(q.toLowerCase()) > -1
                    );
                });
            }
        });
    }

    return (

        <>
            <div>
                <TextField  
                // value={q}
                onChange={(e) => setQ(e.target.value)}
                />
            </div>
            <div>
            <ul>
                {search(items).map((item,key) => (
                    <li key={key}>
                        <h1>{item.NId}</h1>
                        <h3>{item.Hal}</h3>
                        <h4>{item.Nomor}</h4>
                    </li>
                ))}
            </ul>
            
            </div>

        </>
    )
}

export default App2;
