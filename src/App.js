import React,{useState,forwardRef, useEffect} from 'react';
import {
    Container, TextField, AppBar, Box, Tabs, Tab, Grid, Paper, ButtonGroup, Fab, Divider,Typography, 
    Chip, InputBase, FormControl, InputLabel, Link, IconButton, Button, Tooltip, 
    Table,TableCell, TableBody, CircularProgress,TableContainer, TableHead, TableRow, LinearProgress
} from '@material-ui/core';
// import ToggleButton from '@material-ui/lab/ToggleButton';
// import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import PropTypes from 'prop-types';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Select from "react-select";
import SelectSearch from 'react-select-search';
// import {fuzzySearch }  from 'react-select-search';
// import Autocomplete from '@material-ui/lab/Autocomplete';
import Pagination from '@material-ui/lab/Pagination';
// import parse from 'autosuggest-highlight/parse';
// import match from 'autosuggest-highlight/match';
// import MenuIcon from '@material-ui/icons/Menu';
// import SearchIcon from '@material-ui/icons/Search';
// import DirectionsIcon from '@material-ui/icons/Directions';
// import NavigationIcon from '@material-ui/icons/Navigation';
// import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
// import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import EventIcon from '@material-ui/icons/Event';
import AvTimerIcon from '@material-ui/icons/AvTimer';
// import PhoneIcon from '@material-ui/icons/Phone';
// import FavoriteIcon from '@material-ui/icons/Favorite';
import EmailIcon from '@material-ui/icons/Email';
import DeleteIcon from '@material-ui/icons/Delete';
// import StarIcon from '@material-ui/icons/Star';
import TurnedInNotIcon from '@material-ui/icons/TurnedInNot';
import TurnedInIcon from '@material-ui/icons/TurnedIn';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import StarIcon from '@material-ui/icons/Star';
// import PersonPinIcon from '@material-ui/icons/PersonPin';

// import DatePicker from "react-datepicker";
// import "react-datepicker/dist/react-datepicker.css";
// import 'react-datepicker/dist/react-datepicker-cssmodules.css';
import "./style.css";

// import 'rc-datepicker/lib/style.css';
import 'moment/locale/id.js'
import { Datepicker,DatePickerInput } from 'rc-datepicker';
// import ModernDatepicker from 'react-modern-datepicker';
import moment from 'moment';
// import SkeletonWaiting from './component/SkeletonWaiting';

// import myImage1 from './assets/img/asas.jpg';
// import myImage2 from './assets/img/sasa.jpeg';
// import herohead from './assets/img/herohead.png';
// import cloudy from './assets/svg/cloudy.svg';
// import hexagon from './assets/svg/hexagon.svg';
// import searchsikd from './assets/img/searchsikd.png';
// import searchgif from './assets/gif/Search.gif';
// import good1 from './assets/gif/good1.gif';

import notfound2 from './assets/img/page-not-found.png'
// import notfound from './assets/gif/notfound.gif';
import SearchBar from "material-ui-search-bar";


// import TabsContainer  from './component/TabsContainer.js'
import Penting from './component/Penting';
import TogglePenting from './component/TogglePenting';
import { PeopleId, PrimaryRoleId, ApiSearchSurat } from './constants';
import { People } from '@material-ui/icons';


const options = [
    { key: 1, text: 'Choice 1', value: 1 },
    { key: 2, text: 'Choice 2', value: 2 },
    { key: 3, text: 'Choice 3', value: 3 },
  ];




function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
        >
        {value === index && (
            <Box p={3}>
            <Typography>{children}</Typography>
            </Box>
        )}
        </div>
    );
}

TabPanel.propTypes = {
children: PropTypes.node,
index: PropTypes.any.isRequired,
value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
}

const StyledTableCell = withStyles((theme) => ({
    head: {
      // backgroundColor: theme.palette.common.black,
      backgroundColor: '#F9FAFB',
      // color: theme.palette.common.white,
      color: 'black',
      borderTopColor: '#3F51B5',
      borderTopWidth: '4px',
      borderTopStyle: 'solid'
    },
    body: {
      fontSize: 14,
    }
  }))(TableCell);
  
const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
      //   backgroundColor: theme.palette.action.hover,
        backgroundColor: '#D6E4F2',
      },
    },
    hover: {
      backgroundColor: ''
    }
}))(TableRow);

const useStyles = makeStyles((theme) => ({
    appContainer:{
        height: '100%',
        width: '100%',
        backgroundColor: '#ECF0F4'
        // backgroundColor: '#ECF0F4'
    },
    root: {
      flexGrow: 1,
      paddingTop: theme.spacing(2)
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    noBorder: {
        border: "none",
    },
    rootSearch: {
        padding: '20px 12px',
        display: 'flex',
        alignItems: 'center',
        width: 'auto',
      },
    input: {
        marginLeft: theme.spacing(1),
        marginTop: '20px',
        marginBottom: '20px',
        flex: 1,
    },
    button: {
        margin: theme.spacing(1),
        width: '100%'
    },
    iconButton: {
        padding: 10,
    },
    divider: {
        height: 28,
        margin: 4,
    },

    extendedIcon: {
        marginLeft: theme.spacing(1),
      },
    arrow: {
        textAlign: 'center',
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'center',
    },
    table: {
        paddingTop: theme.spacing(5)
    },
    pagination: {
        paddingTop: theme.spacing(5),
        textAlign: 'center'
    },
    rootTab: {
        flexGrow: 1,
        width: 'auto',
        textAlign: 'center'
    },
    table: {
        minWidth: 700,
    },
    read: {
        fontSize: 12,
    
    },
    unread: {
        fontSize: 12,
        fontWeight: 'bold'
    },
    tablerow: {
        backgroundColor: '#ECF0F4'
    }
}));

const LoadingList = () => {
    return (
        <StyledTableRow style={{width: 1200, backgroundColor: '#DEE3F1', height: 500, opacity:.5}}>
            <StyledTableCell  colSpan='8' align="center" style={{width: '700px'}}>
                <Typography variant="h1">
                    <CircularProgress />
                    {/* <img src={searchgif} style={{height: 'auto', width: '60%'}}/> */}
                    {/* <Typography>Searching data what you want </Typography> */}
                </Typography>
            </StyledTableCell>
        </StyledTableRow>
    )
}

const NotFound = () => {
    return (
        // <StyledTableRow style={{width: 1200, backgroundColor: '#F0F4F8', height: 500}}>
        <StyledTableRow style={{width: 1200, backgroundColor: '#fff', height: 500}}>
            <StyledTableCell  colSpan='8' align="center" style={{width: '700px'}}>
                <Typography variant="h1">
                    <img src={notfound2} style={{height: 'auto', width: '30%'}}/>
                </Typography>
                <Typography variant="subtitle1" style={{color: '#617E96'}}> Maaf, surat tidak ditemukan</Typography>
            </StyledTableCell>
        </StyledTableRow>
    )
}



const limit = (string, limit) => {
    return string.substring(0, limit)
}

function App(props){
    const classes = useStyles();
    const perops = props;
    const windows = window;
    // const PrimaryRoleIdTes = PrimaryRoleId == undefined || PrimaryRoleId == null ? window.primaryroleid1 : 
    console.log('PRIMARYROLEID', PrimaryRoleId)
    console.log('PrimaryRoleIdTes', window)
    console.log('PEOPLEID', PeopleId)
    const [roleid, setRoleid] = useState(PrimaryRoleId);

    const [statusNaskah, setStatusNaskah] = useState('semua');
    const [sumber, setSumber] = React.useState('ternal');
    const [selectedUnitKerja, setSelectedUnitKerja] = useState(null);
    const [search, setSearch] = useState('hal');
    const [query,setQuery] = useState('');
    const [store, setStore] = useState('');
    const [page, setPage] = React.useState(1);
    const [totalPage, setTotalPage] = useState(null);
    const [date, setDate] = useState(moment().format('YYYY-MM-DD'))
    const [loading, setLoading] = useState(true);
    const [alignment, setAlignment] = React.useState('1');
    const [notFound, setNotFound] = useState(false)
    const [peopleId, setPeopleId] = useState(PeopleId);
    const [primaryRoleId, setPrimaryRoleId] = useState(PrimaryRoleId);
    const [data, setData] = useState(null);
    const [searchTerm, setSearchTerm] = React.useState("");
    const [loadPaginate, setLoadPaginate] = useState(false)
     
    const [pageSize, setPageSize] = useState(10)
  
    const lastMonth = {
        startDate2: moment(date).add(-12, 'M').format('YYYY-MM-DD'),
        endDate2: date
    }

    const [startDate2, setStartDate2] = useState(lastMonth.startDate2);
    const [endDate2, setEndDate2] = useState(lastMonth.endDate2);

    const [valueAppBar, setValueAppBar] = useState(0)
    const handleChangeAppBar = (event, newValue) => {
        setValueAppBar(newValue);
      };

    const handleChangeStart = (date) => {
        setStartDate2(date)
    }
    const onchangeSelect = (item) => {
        setCurrentCountry(null);
        setRegion(item);
    };
    const handlePage = (event, value) => {
        setPage(value);
        setLoadPaginate(true)
        
      };
    const CustomButtonDateBegin = forwardRef(({ value, onClick }, ref) => (
        <Fab 
            onClick={onClick} 
            ref={ref}  
            variant="extended" 
            color="primary" 
            aria-label="add" 
            className={classes.button}
            style={{ width: '100%'}}
        >
            {value || 'Mulai'}
            <EventIcon className={classes.extendedIcon} />
        </Fab>
      ));
    const CustomButtonDateEnd = forwardRef(({ value, onClick }, ref) => (
        <Fab 
            onClick={onClick} 
            ref={ref}  
            variant="extended" 
            color="primary" 
            aria-label="add" 
            className={classes.button}
            style={{ width: '100%'}}
        >
            {value || 'Akhir'}
            <EventIcon className={classes.extendedIcon} />
        </Fab>
    ));

    

    

    const handleAlignment = (event, newAlignment) => {
      setAlignment(newAlignment);
    };
  
      
    const handleDelete = () => {
        console.info('You clicked the delete icon.');
    };
    
    useEffect(() => {
        const fetchMonth = async() => {
            try{
                const lastMonth = {
                    startDate2: moment(date).add(-12, 'M').format('YYYY-MM-DD'),
                    endDate2: date
                }
            
                const threeMonth = {
                    startDate2: moment(date).add(-3, 'M').format('YYYY-MM-DD'),
                    endDate2: date
                }
            
                const sixMonth = {
                    startDate2: moment(date).add(-6, 'M').format('YYYY-MM-DD'),
                    endDate2: date
                }
                alignment === 1 ? setAlignment(1) : 
                alignment === 2 ? setAlignment(2) :
                alignment === 3 
                if(alignment == 1){
                    setStartDate2(lastMonth.startDate2);
                    setEndDate2(lastMonth.endDate2)
                } else if(alignment == 2){
                    setStartDate2(threeMonth.startDate2);
                    setEndDate2(threeMonth.endDate2)
                } else {

                }
            } catch (e) {
                console.log(e)
            }
        }
        fetchMonth()
    },[alignment])
    

    useEffect(() => {
        const fetchUnitKerja = async() => {
            try{
                const apiUnitKerja = `https://sikd_jamal.mkri.id/php-rest-api2/api/mailinbox/unit_kerja.php`;
                await fetch(apiUnitKerja)
                    .then((responses) => {
                        return responses.json();
                    })
                    .then((resp) => {
                        const dataMentahUnitKerja = resp.data;
                        var UnitKerja = [];
                        var objUnitKerja = {};
                        
                        for(var i = 0 ; i<dataMentahUnitKerja.length; i++){
                            var obj = dataMentahUnitKerja[i];

                            var objUnitKerja = {
                                value: obj.RoleId,
                                label: obj.RoleName
                            }

                            UnitKerja[i]=objUnitKerja;
                        }

                        setUnitKerja(UnitKerja)
                    })
            } catch{

            }
        }
        fetchUnitKerja();
    },[])



    useEffect(() => {
        
        const timeout = setTimeout(async() => {
            try {
                console.log('PrimaryRoleIdTESSS in useeffect', window)
                const bodyDummy = {
                    people_id   : PeopleId,
                    role_id     : PrimaryRoleId,
                    sumber      : "semua",
                    unitkerja   : "",
                    query       : query,
                    date1       : `${startDate2} 00:00:00`,
                    date2       : `${endDate2} 23:59:59`,
                    page        : page,
                    page_size   : pageSize
                }
                const fetchSearchSurat = await fetch(ApiSearchSurat, {
                    method: 'POST',
                    mode: 'cors',
                    headers: {"Content-type": "application/json"},
                    body: JSON.stringify(bodyDummy)
                })
                const resultSearchSurat = await fetchSearchSurat.json()
                setNotFound(resultSearchSurat.results == null ? true : false)
                setData(resultSearchSurat.results)
                setLoading(false)
                setPage(resultSearchSurat.totalPages > 1 ? page : 1)
                setTotalPage(resultSearchSurat.totalPages)
                setPeopleId(PeopleId)
                setPrimaryRoleId(PrimaryRoleId)
                setLoadPaginate(false)
                console.log('resultSearchSurat',resultSearchSurat)

            } catch(e){
                console.log(e);
                setLoading(false)
                setLoadPaginate(false)
            }



            // const api = `https://sikd_jamal.mkri.id/php-rest-api2/api/mailinbox/read.php?`;
           
            // setLoading(true)
			// try{
            //     const apiTest = `${api}read=${statusNaskah}&sumber=${sumber}&begin=${page}&row=10&primaryroleid=${PrimaryRoleId}&unit=${selectedUnitKerja == null ? '': selectedUnitKerja}&sort=DESC&search=${search}&q=${query}&tgl1="${startDate2} 00:00:00"&tgl2="${endDate2} 23:59:59"`;
            //     console.log('selectedUnitKerja', selectedUnitKerja)
            //     await fetch(apiTest)
			// 		.then((responses) => {
			// 			return responses.json();
			// 		})
			// 		.then((resp) => {
            //             const dataMentah = resp.data
            //             setNotFound(resp.data === "Not Found" ? true : false)
            //             var dataSiap = [];
                        

            //             for(var i = 0 ; i < dataMentah.length ; i ++){
            //                 var obj = dataMentah[i]
            //                 dataSiap[i] = obj
            //             }

            //             // console.log('dataSiap', dataSiap)
            //             setTotalPage(resp.total_pages)
            //             setData(dataSiap)
            //             const results = dataSiap.filter(item => 
            //                 item.Hal.toLowerCase().includes(searchTerm) ||
            //                 item.Nomor.toLowerCase().includes(searchTerm) 
            //               );
            //             setSearchResults(results)
            //             setLoading(false)
            //             setPage(resp.total_pages > 1 ? page : 1)
            //             setPeopleId(PeopleId)
            //             setPrimaryRoleId(PrimaryRoleId)
                        
            //             // resp.total_pages === '1' ? setPage(1) : page;
            //             // console.log('resultsss:', results)
                        
						
			// 		})
			// } catch(err){
			// 	console.log(err);
            //     setLoading(false)
			// }
        }, 350)
		return () => clearTimeout(timeout)
	}, [roleid, query, sumber,store, statusNaskah,selectedUnitKerja, search,  page , startDate2, endDate2])


    const handleChangeSearch = e => {
        setSearchTerm(e.target.value);
      };
    const handleStatusNaskah = (e) => {
        setStatusNaskah(e)
    }
    const handleSumber = (e) => {
        e === 'ternal' ? setSumber(e) :console.log('false');
        setSumber(e)
    }



    return(
        <>
            <div className={classes.appContainer}>
            
            <div className={classes.root}>
                <Grid container spacing={1} direction="row"
                    justify="center"
                    alignItems="center">
                    {/* <Grid item xs={12}>
                        <Paper component="form" className={classes.rootSearch}>
                            <InputBase
                                className={classes.input}
                                placeholder="Search Here"
                                inputProps={{ 'aria-label': 'search here' }}
                            />
                            <Divider className={classes.divider} orientation="vertical" />
                            <IconButton type="submit" className={classes.iconButton} aria-label="search">
                                <SearchIcon />
                            </IconButton>
                        </Paper>
                    </Grid> */}
                    {/* <Grid item xs={12}>
                    <div className="ui right labeled input">
                    <input type="text" placeholder="Find domain"/>
                    <div className="ui dropdown label">
                        <div className="text">.com</div>
                        <i className="dropdown icon"></i>
                        <div className="menu">
                        <div className="item">.com</div>
                        <div className="item">.net</div>
                        <div className="item">.org</div>
                        </div>
                    </div>
                    </div>
                    </Grid> */}
                    <Grid item xs={12}>
                        {/* <img src={herohead} style={{height:'350px' ,width: 'auto', borderRadius: '50px', marginBottom: '-300px' , boxShadow: '0 0.0625rem 0.125rem rgba(0, 0, 0, 0.15)'}}/> */}
                        <div>
                            {/* <img src={hexagon} style={{height:'auto' ,width: '100%', borderRadius: '50px', marginBottom: '-310px' , boxShadow: '0 0.0625rem 0.125rem rgba(0, 0, 0, 0.15)'}}/> */}
                            <img src="" style={{height:'auto' ,width: '100%', borderRadius: '50px', marginBottom: '-310px' , boxShadow: '0 0.0625rem 0.125rem rgba(0, 0, 0, 0.15)'}}/>

                        </div>
                    </Grid>
                    <Grid item xs={12}>
                        <div style={{textAlign: 'center', color:'#white'}}>
                        {/* <img src={searchsikd} style={{height:'100px' ,width: 'auto', paddingTop: '1em', paddingBottom:'1em',borderRadius: '50px', }}/> */}
                            {/* <Typography variant="h5" style={{color:'#black', fontWeight: 'bold', paddingTop: '1em', paddingBottom:'1em'}} gutterBottom>What do you want to find? Find it</Typography> */}
                        </div>
                    </Grid>
                    <Grid item xs={12} style={{paddingLeft: '2.5em', paddingRight: '2.5em', paddingBottom: '2em'}}>
                        <SearchBar
                            value={""}
                            onClick={() => setPage(1)}
                            onChange={(newValue) => {
                                setQuery(newValue),
                                setLoadPaginate(true)
                            }}
                            onRequestSearch={() => console.log('sad')}
                            onCancelSearch={() => {
                                setQuery('')
                                setLoadPaginate(true)
                            }}
                            placeholder="Cari berdasarkan Nomor Naskah, Asal Naskah, Pengirim Naskah, Pesan atau Hal"
                            style={{ height: '60px', borderRadius: '25px', boxShadow: 'rgba(0, 0, 0, 0.2) 0px 3px 1px -2px, rgba(0, 0, 0, 0.14) 0px 2px 2px 0px, rgba(0, 0, 0, 0.12) 0px 1px 5px 0px'}}
                        />
                    </Grid>
                    {/* <Grid item xs={12} style={{paddingLeft: '2.5em', paddingRight: '2.5em'}}>
                    <div className="ui fluid action input">
                        <input 
                            style={{height: '60px', 
                            borderTopLeftRadius: '25px', 
                            borderBottomLeftRadius: '25px', 
                            boxShadow: '0 .5rem 1rem rgba(0,0,0,.15)'}}
                            type="text" 
                            placeholder="Search Unit Kerja" 
                            onChange={(e) => {setQuery(e.target.value)}}
                        />
                        <select 
                        style={{height: '60px' , borderLeftColor:'none', borderLeftWidth: '0', borderRightWidth: '0',
                        boxShadow: '0 .5rem 1rem rgba(0,0,0,.15)'
                        }}
                        className="ui compact selection dropdown" 
                        value={search} 
                        onChange={(e) => {setSearch(e.target.value)}}>
                            <option  value="hal">Hal</option>
                            <option  value="nomornaskah">Nomor Naskah</option>
                            <option  value="sumbernaskah">Sumber Naskah</option>
                        </select>
                        <div 
                            style={{height: '60px', width: '15%', 
                            boxShadow: '0 .5rem 1rem rgba(0,0,0,.15)',
                            borderTopRightRadius: '25px', 
                            borderBottomRightRadius: '25px'}}
                            className="ui button" 
                            onClick={() => setStore(query)}
                        >
                        <Typography variant="h6" style={{ fontWeight: 'bold'}}>Search</Typography>
                        
                        </div>
                    </div>
                    </Grid> */}
                    
                    {/* <Grid item xs={2}>
                        <div style={{width: '100%', textAlign:'center'}}>
                            <DatePicker
                                selected={startDate}
                                onChange={(date) => {
                                    setStartDate(date);
                                    console.log(date)
                                }}
                                customInput={<CustomButtonDateBegin />}
                                style={{width: '100%'}}
                            />
                            
                        </div>
                    </Grid>
                    <Grid item xs={1} className={classes.arrow} >
                        <ArrowForwardIcon  />
                    </Grid>
                    <Grid item xs={2}>
                        <div style={{width: '100%', textAlign:'center'}}>
                            <DatePicker
                                selected={endDate}
                                onChange={(date) => {
                                    setEndDate(date);
                                    console.log(date)
                                }}
                                customInput={<CustomButtonDateEnd />}
                                style={{width: '100%'}}
                            />
                            
                        </div>
                    </Grid>
                    <Grid item xs={2}>
                        <div>
                            <Fab variant="extended" color="primary"  aria-label="add" className={classes.button}>
                                Pilih Sumber
                                <ArrowDropDownIcon className={classes.extendedIcon} />
                            </Fab>
                        </div>
                    </Grid>
                    <Grid item xs={2}>
                        <div>
                            <Fab variant="extended" color="primary"  aria-label="add" className={classes.button}>
                                Sumber
                                <ArrowDropDownIcon className={classes.extendedIcon} />
                            </Fab>
                        </div>
                    </Grid>
                     */}



                    {/* <Grid item xs={2}>
                        <div>
                        <Select2
                            style={{padding: '8px'}}
                            value={sumber}
                            onChange={onchangeSelect}
                            options={options2}
                            getOptionValue={(option) => option.value}
                            getOptionLabel={(option) => option.value}
                        />
                        </div>
                    </Grid> */}
                    <Grid item xs>
                        <div style={{paddingLeft: '2.5em', }}>
                        <Typography variant="subtitle2" style={{color:'rgba(0, 0, 0, 0.64)', fontWeight: 'bold'}}>Status Naskah</Typography>
                        <SelectSearch value={statusNaskah}  onChange={handleStatusNaskah} options={[
                            {name: 'Semua', value: 'semua'},
                            {name: 'Read', value: 'read'},
                            {name: 'Unread', value: 'unread'},
                        ]}name="language" placeholder="Pilih Status Naskah" />
                        </div>
                    </Grid>
                    
                    <Grid item xs>
                        <div style={{paddingRight: '2.5em', }}>
                        <Typography variant="subtitle2" style={{color:'rgba(0, 0, 0, 0.64)', fontWeight: 'bold'}}>Sumber Naskah</Typography>
                        <SelectSearch value={sumber}  onChange={handleSumber} options={[
                            {name: 'Semua', value: 'ternal'},
                            {name: 'Internal', value: 'internal'},
                            {name: 'External', value: 'external'},
                        ]}  name="language" placeholder="Pilih Sumber Naskah" />
                        </div>
                    </Grid>
                   {/* <Grid item xs>
                        <div style={{ paddingRight: '2.5em'}}>
                            <Typography variant="subtitle2" style={{fontWeight: 'bold'}}>Unit Kerja</Typography>
                            <Select
                                defaultValue={selectedUnitKerja}
                                onChange={(e) => setSelectedUnitKerja(e.value)}
                                options={unitKerja}
                            />
                        </div>
                   </Grid> */}
                   
                    <Grid item xs={12} style={{paddingTop: '1.5em'}}>
                        <Grid container direction="row"
                            justify="center"
                            alignItems="center">
                            <Grid item xs={12}  >
                                
                                <Typography variant="subtitle2" gutterBottom style={{color:'rgba(0, 0, 0, 0.64)', fontWeight: 'bold', textAlign: 'center'}}>Pencarian Sejak:</Typography>
                            </Grid>
                            <Grid item xs={2}>
                                <div style={{width: '100%', textAlign:'center'}}>
                                    {/* <DatePicker
                                        selected={startDate}
                                        onChange={(date) => {
                                            setStartDate(date);
                                            console.log(date)
                                        }}
                                        customInput={<CustomButtonDateBegin />}
                                        style={{width: '100%'}}
                                    /> */}
                                     <DatePickerInput
                                        displayFormat='DD/MM/YYYY'
                                        returnFormat='YYYY-MM-DD'
                                        className='my-react-component'
                                        value={startDate2}
                                        defaultValue={lastMonth.startDate2}
                                        // valueLink={(e) => console.log(e)}
                                        onChange={e => {setStartDate2(moment(e).format('YYYY-MM-DD'))}}
                                        showOnInputClick
                                        placeholder='Tanggal Mulai'
                                        locale='id'
                                        iconClassName="icon-rc-datepicker icon-rc-datepicker_calendar"
                                        style={{color:'rgba(0, 0, 0, 0.64)'}}
                                    />
                                    
                                </div>
                            </Grid>
                            <Grid item xs={1} className={classes.arrow} >
                                <ArrowForwardIcon  style={{color:'rgba(0, 0, 0, 0.64)',}} />
                            </Grid>
                            
                            <Grid item xs={2}>
                                <div style={{width: '100%', textAlign:'center'}}>
                                    {/* <DatePicker
                                        selected={endDate}
                                        onChange={(date) => {
                                            setEndDate(date);
                                            console.log(date)
                                        }}
                                        customInput={<CustomButtonDateEnd />}
                                        style={{width: '100%'}}
                                    /> */}
                                     <DatePickerInput
                                        displayFormat='DD/MM/YYYY'
                                        returnFormat='YYYY-MM-DD'
                                        className='my-react-component'
                                        value={endDate2}
                                        defaultValue={lastMonth.endDate2}
                                        // valueLink={(e) => console.log(e)}
                                        onChange={e => {setEndDate2(moment(e).format('YYYY-MM-DD'))}}
                                        showOnInputClick
                                        placeholder='Tanggal Akhir'
                                        locale='id'
                                        iconClassName="icon-rc-datepicker icon-rc-datepicker_calendar"
                                    />
                                </div>
                            </Grid>
                            
                        </Grid>
                    </Grid>
                    <Grid item xs={12} style={{paddingTop: '2.5em'}}>
                        <Grid container>
                            {/* <Grid item xs={7}>
                                <div style={{textAlign: 'left'}}>
                                    <Typography 
                                        variant="h6" 
                                        style={{color: 'rgba(0, 0, 0, 0.74)',fontSize: '12px', fontWeight: 'bold'}} 
                                        gutterBottom
                                    >
                                        Pencarian Sejak:
                                    </Typography>
                                    
                                    <Chip 
                                        label={moment(startDate2).format('LL')}
                                        style={{
                                            fontSize: '14px',
                                            fontWeight: 'bold',
                                            letterSpacing: '2px',
                                            color: 'rgba(0, 0, 0, 0.64)'
                                        }}
                                    />
                                    <Typography display="inline" style={{fontWeight: 'bold', fontSize: '11px', paddingLeft: '1em', paddingRight: '1em'}}>s/d</Typography>
                                    <Chip 
                                        label={moment(endDate2).format('LL')}
                                        style={{
                                            fontSize: '14px',
                                            fontWeight: 'bold',
                                            letterSpacing: '2px',
                                            color: 'rgba(0, 0, 0, 0.64)'
                                        }}
                                    />

                                </div>
                            </Grid> */}
                            {/* <Grid item xs={5} style={{textAlign: 'right',  }}>
                                <ToggleButtonGroup
                                    value={alignment}
                                    exclusive
                                    onChange={handleAlignment}
                                    aria-label="text alignment"
                                    style={{
                                        backgroundColor: 'white', 
                                        textAlign: 'center' ,
                                        boxShadow: '0px 2px 1px -1px rgba(0,0,0,0.2),0px 1px 1px 0px rgba(0,0,0,0.14),0px 1px 3px 0px rgba(0,0,0,0.12)'
                                    }}
                                >
                                    <ToggleButton value="1" aria-label="left aligned">
                                        <Typography variant="subtitle2" style={{fontSize: '12px', fontWeight: 'bold'}} >Last month</Typography>
                                    </ToggleButton>
                                    <ToggleButton value="2" aria-label="centered">
                                        <Typography variant="subtitle2" style={{fontSize: '12px', fontWeight: 'bold'}} >Last 3 month</Typography>
                                    </ToggleButton>
                                    <ToggleButton value="3" aria-label="right aligned">
                                        <Typography variant="subtitle2" style={{fontSize: '12px', fontWeight: 'bold'}} >Custom</Typography>
                                    </ToggleButton>
                                </ToggleButtonGroup>
                            </Grid> */}
                        </Grid>
                        
                    </Grid>
                    {/* <Grid item xs={6} style={{textAlign: 'right',  paddingTop: '30px'}}>
                        <div style={{textAlign: 'center'}}>
                            <Typography variant="h5" gutterBottom>Pesan Masuk Sejak:</Typography>
                            <Typography variant="h5" style={{fontStyle:'italic',textTransform:'uppercase', letterSpacing: '5px'}}>1 Januari 2021 s/d 28 Juni 2021</Typography>

                        </div>
                    </Grid> */}
                    <Grid item xs={12}>
                        <div>
                            <AppBar position="static">
                                <Tabs variant="fullWidth" value={valueAppBar} onChange={handleChangeAppBar} aria-label="simple tabs example">
                                <Tab icon={<EmailIcon />} label="Semua Naskah" {...a11yProps(0)} />
                                <Tab icon={<StarIcon />} label="Ingatkan" {...a11yProps(1)} />
                                </Tabs>
                            </AppBar>
                            <TabPanel value={valueAppBar} index={0}>
                                <TableContainer component={Paper}>
                                    {loading == true ? <LinearProgress style={{marginBottom: '-.4em'}} /> : '' }
                                    <Table className={classes.table} aria-label="customized table">
                                        {loading == true ? '' : notFound == true ? '' : 
                                            <TableHead>
                                                <TableRow>
                                                    <StyledTableCell align="center"  style={{}}>Ingatkan</StyledTableCell>
                                                    <StyledTableCell align="left"  style={{}}>Status Naskah</StyledTableCell>
                                                    {/* <StyledTableCell align="left"  style={{}}>Nomor Naskah</StyledTableCell> */}
                                                    <StyledTableCell align="left"  style={{}}>Asal Naskah</StyledTableCell>
                                                    <StyledTableCell align="left"  style={{}}>Pengirim</StyledTableCell>
                                                    <StyledTableCell align="left"  style={{}}>Hal</StyledTableCell>
                                                    {/* <StyledTableCell align="left"  style={{}}>Pesan</StyledTableCell> */}
                                                    <StyledTableCell align="left"  style={{}}>Tanggal</StyledTableCell>
                                                    {/* <StyledTableCell align="left"  style={{}}>Tanggal Registrasi</StyledTableCell> */}
                                                    {/* <StyledTableCell align="left"  style={{}}>DS</StyledTableCell> */}
                                                </TableRow>
                                            </TableHead>
                                        }
                                        
                                        <TableBody style={{opacity: loadPaginate == true ? .4 : 1}}>
                                        {/* <LoadingList/> */}
                                        {/* <NotFound/> */}
                                        {loading == true ? <LoadingList/> : notFound == true ? <NotFound/> : data&&data.map((row, key) => (
                                            <StyledTableRow key={key} hover={true} className={classes.tablerow} >
                                                <StyledTableCell  className={row.StatusReceive == 'unread' ? classes.unread : classes.read} align="center" style={{ fontWeight: 'bold', fontSize: '12px'}}> 
                                                    <TogglePenting nid={row.NId} primaryRoleId={row.RoleId_To} peopleId={row.To_Id} data={row}/> 
                                                </StyledTableCell>
                                                <StyledTableCell  className={row.StatusReceive == 'unread' ? classes.unread : classes.read} align="left" style={{}}>
                                                    {/* <Link href={`https://sikd.mkri.id/index3.php?option=MailTL&id=${row.NId}`} className={row.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit"> */}
                                                        <Typography variant='h6' style={{fontWeight: 'bold', fontSize: 12}}>Jenis Surat :</Typography>
                                                        <Chip size="small" label={row.jenis_naskah} style={{backgroundColor: 'amber'}} color={row.jenis_naskah === 'Disposisi' ? 'Secondary' : row.jenis_naskah === 'Nota Dinas' ? 'Primary' : ''}/>
                                                        <Typography variant='h5' style={{fontWeight: 'bold', fontSize: 12, paddingTop: '1em'}}>Baca :</Typography>
                                                        <Chip size="small" label={row.StatusReceive == 'unread' ? 'Belum Dibaca' : 'Sudah Dibaca'} color={row.StatusReceive == 'unread' ? 'secondary' : '' } />
                                                        <Typography variant='h5' style={{fontWeight: 'bold', fontSize: 12, paddingTop: '1em'}}>Response Time :</Typography>
                                                        <Chip size="small" label={row.StatusReceive == 'unread' ? row.differences_text : row.differences_text} color={row.StatusReceive == 'unread' ? 'secondary' : ''} />
                                                        { 
                                                            row.StatusReceive === 'unread' ?
                                                                <Tooltip title="Pesan yang belum di respon akan menurunkan Response Time anda.." placement="right-end">
                                                                    <IconButton aria-label="Pesan yang belum di respon akan menurunkan Response time anda..">
                                                                    <AvTimerIcon color='secondary' fontSize="small"/>
                                                                    </IconButton>
                                                                </Tooltip> : ''
                                                        }
                                                        
                                                    {/* </Link> */}
                                                </StyledTableCell>
                                                {/* <StyledTableCell  className={row.StatusReceive == 'unread' ? classes.unread : classes.read} align="left" style={{}}>
                                                    <Link href={`https://sikd.mkri.id/index3.php?option=MailTL&id=${row.NId}`} className={row.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit">{row.Nomor}</Link>
                                                </StyledTableCell> */}
                                                <StyledTableCell  className={row.StatusReceive == 'unread' ? classes.unread : classes.read} align="left" style={{}}>
                                                    <Link href={`https://sikd.mkri.id/index3.php?option=MailTL&id=${row.NId}`} className={row.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit">
                                                      
                                                        <Typography variant='h2' style={{fontSize: 12, fontWeight: row.StatusReceive === 'unread' ? 'bold' : ''}}>{row.asal_naskah}</Typography>
                                                    </Link>
                                                </StyledTableCell>
                                                <StyledTableCell  className={row.StatusReceive == 'unread' ? classes.unread : classes.read} align="left" style={{}}>
                                                    <Link href={`https://sikd.mkri.id/index3.php?option=MailTL&id=${row.NId}`} className={row.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit">
                                                        <Typography variant='caption' style={{fontSize: 12, fontWeight: row.StatusReceive === 'unread' ? 'bold' : ''}}>{row.nama_pengirim_sql}</Typography>
                                                        <Typography variant='h2' style={{fontSize: 12, fontWeight: row.StatusReceive === 'unread' ? 'bold' : ''}}>{row.jabatan_pengirim_sql}</Typography>
                                                    </Link>
                                                </StyledTableCell>
                                                <StyledTableCell  className={row.StatusReceive == 'unread' ? classes.unread : classes.read} align="left" style={{}}>
                                                    <Link href={`https://sikd.mkri.id/index3.php?option=MailTL&id=${row.NId}`} className={row.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit">
                                                        <Typography variant='caption' style={{fontWeight: 'bold',fontSize: 12}}>Nomor Naskah :</Typography>
                                                        <Typography variant='h2' style={{fontSize: 12, fontWeight: row.StatusReceive === 'unread' ? 'bold' : ''}}>{row.Nomor === '' ? '-' : row.Nomor}</Typography>

                                                        <Typography variant='caption' style={{fontWeight: 'bold',fontSize: 12, paddingTop: '1em'}}>Hal :</Typography>
                                                        <Typography variant='h2' style={{fontSize: 12, fontWeight: row.StatusReceive === 'unread' ? 'bold' : ''}}>{row.Hal}</Typography>

                                                        <Typography variant='caption' style={{fontWeight: 'bold',fontSize: 12, paddingTop: '.7em'}}>Pesan :</Typography>
                          
                                                        <Typography variant='h2' style={{fontSize: 12, fontWeight: row.StatusReceive === 'unread' ? 'bold' : ''}}>{limit(row.Msg, 100)} ...</Typography>
                                                    </Link>
                                                </StyledTableCell>
                                                {/* <StyledTableCell  className={row.StatusReceive == 'unread' ? classes.unread : classes.read} align="left" style={{}}>
                                                    <Link href={`https://sikd.mkri.id/index3.php?option=MailTL&id=${row.NId}`} className={row.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit">{row.Msg}</Link>
                                                </StyledTableCell> */}
                                                <StyledTableCell  className={row.StatusReceive == 'unread' ? classes.unread : classes.read} align="left" style={{}}>
                                                    <Link href={`https://sikd.mkri.id/index3.php?option=MailTL&id=${row.NId}`} className={row.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit">
                                                        <Typography variant='caption' style={{fontWeight: 'bold', fontSize: 12}}>Tanggal Registrasi:</Typography>
                                                        <Typography variant='h2' style={{fontSize: 12}}>{moment(row.Tgl).format('YYYY-MM-DD')}</Typography>

                                                        <Typography variant='caption' style={{fontWeight: 'bold', fontSize: 12, paddingTop: '1em'}}>Tanggal Diterima:</Typography>
                                                        <Typography variant='h2' style={{fontSize: 12}}>{moment(row.ReceiveDate).format('YYYY-MM-DD HH:mm:ss')}</Typography>
                                                        <Chip label={moment(row.ReceiveDate).startOf('second').fromNow()} size="small" style={{backgroundColor: '#ffc107'}}/>
                                                    </Link>
                                                </StyledTableCell>
                                                {/* <StyledTableCell   className={row.StatusReceive == 'unread' ? classes.unread : classes.read} align="left" style={{}}>
                                                    <Link href={`https://sikd.mkri.id/index3.php?option=MailTL&id=${row.NId}`} className={row.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit">{row.ReceiveDate}</Link>
                                                </StyledTableCell> */}
                                                {/* <StyledTableCell  className={row.StatusReceive == 'unread' ? classes.unread : classes.read} align="left" style={{}}>
                                                    <Link href={`https://sikd.mkri.id/index3.php?option=MailTL&id=${row.NId}`} className={row.StatusReceive == 'unread' ? classes.unread : classes.read} color="inherit">{row.ds}</Link>
                                                </StyledTableCell> */}
                                            </StyledTableRow>
                                            
                                        ))}
                                    
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                                {notFound || loading == true 
                                    ? '' 
                                    : 
                                    <div className={classes.pagination}>
                                        <Grid container direction="row" justify="center"  alignItems="center">
                                            <Grid item>
                                                {/* <Pagination count={30} color="primary" /> */}
                                                <Pagination count={totalPage > 1 ? totalPage : 1 } page={totalPage > 1 ? page : 1} onChange={handlePage} color="primary"/>
                                            </Grid>
                                        </Grid>
                                    </div>
                                }
                                





                                
                                {/* <div className={classes.table}>
                
                                    {loading ? <LinearProgress style={{marginBottom: '-1.2em'}}/> : ''}

                                    <table class="ui striped blue table">
                                        <thead>
                                            <tr>
                                                <th><Typography width="5%" variant="subtitle1" style={{fontSize: '12px', fontWeight: 'bold'}}>Tandai</Typography></th>
                                                <th><Typography width="5%" variant="subtitle1" style={{fontSize: '12px', fontWeight: 'bold'}}>Status Naskah</Typography></th>
                                                <th><Typography width="14%" variant="subtitle1" style={{fontSize: '12px', fontWeight: 'bold'}}>Nomor Naskah</Typography></th>
                                                <th><Typography width="20%" variant="subtitle1" style={{fontSize: '12px', fontWeight: 'bold'}}>Asal Naskah</Typography></th>
                                                <th><Typography width="35%" variant="subtitle1" style={{fontSize: '12px', fontWeight: 'bold'}}>Hal</Typography></th>
                                                <th><Typography width="7%" variant="subtitle1" style={{fontSize: '12px', fontWeight: 'bold'}}>Tanggal Naskah</Typography></th>
                                                <th><Typography width="7%" variant="subtitle1" style={{fontSize: '12px', fontWeight: 'bold'}}>Tanggal Registrasi</Typography></th>
                                                <th><Typography width="7%" variant="subtitle1" style={{fontSize: '12px', fontWeight: 'bold'}}>DS</Typography></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {data ? 
                                                data.map((row, key) => (
                                                    <tr key={key}>
                                                        <td width="5%"><Typography variant="subtitle1" style={{fontSize: '12px'}}><TurnedInNotIcon /></Typography></td>
                                                        <td width="5%"><Typography variant="subtitle1" style={{fontSize: '12px'}}>{row.StatusReceive}</Typography></td>
                                                        <td width="14%"><Typography variant="subtitle2" style={{fontSize: '12px'}}>{row.Nomor}</Typography></td>
                                                        <td width="20%"><Typography variant="subtitle1" style={{fontSize: '12px'}}>{row.JabatanPengirim ? row.JabatanPengirim : row.zz}</Typography></td>
                                                        <td width="35%"><Typography variant="subtitle1" style={{fontSize: '12px'}}>{row.Hal}</Typography></td>
                                                        <td width="7%"><Typography variant="subtitle1" style={{fontSize: '12px'}}>{row.Tgl}</Typography></td>
                                                        <td width="7%"><Typography variant="subtitle1" style={{fontSize: '12px'}}>{row.ReceiveDate}</Typography></td>
                                                        <td width="7%"><Typography variant="subtitle1" style={{fontSize: '12px'}}>{row.ds}</Typography></td>
                                                    </tr>
                                                ))
                                                :
                                                (
                                                <Grid container>
                                                    <Grid item xs={12} style={{width: '100%'}}>
                                                        <SkeletonWaiting/>

                                                    </Grid>
                                                </Grid>
                                                
                                                )
                        
                                                
                                            }
                                            
                                        </tbody>
                                    </table> 
                               
                            </div> */}
                            </TabPanel>
                            <TabPanel value={valueAppBar} index={1}>
                                <Penting data={data} primaryRoleId={primaryRoleId} peopleId={peopleId} />
                            </TabPanel>
                        </div>
                    </Grid>
                    
                    {/* <Grid item xs={12} style={{paddingTop: '1em'}}>
                        <Paper square className={classes.rootTab}>
                            <Tabs
                                value={tab}
                                onChange={(event, newValue) => {
                                    setTab(newValue);
                                    console.log('newValue')
                                  }}
                                variant="fullWidth"
                                indicatorColor="primary"
                                textColor="primary"
                                aria-label="icon label tabs example"
                            >
                                <Tab icon={<EmailIcon />} label="SEMUA PESAN" />
                                <Tab icon={<TurnedInIcon />} label="PENTING" />
                            </Tabs>
                        </Paper>
                    </Grid>

                    <Grid item xs={12}>
                    
                        
                    </Grid> */}
                   
                </Grid>
                
            </div>
            
           
           
            </div>
        </>
       
    );
}

const ValueFormatter = (value) => {
    let totalSeconds = value
    let days = Math.floor(totalSeconds / 86400)
    totalSeconds -= days * 86400
    let hours = Math.floor(totalSeconds / 3600) % 24
    totalSeconds -= hours * 3600
    let minutes = Math.floor(totalSeconds / 60) % 60
    totalSeconds -= minutes * 60
    let seconds = totalSeconds % 60
    const toReturn = (days + " Hari " + hours + " Jam " + minutes + " Menit" + seconds + " Detik")
    return  toReturn
}

// const ResponseTimeUnread = (ReceiveDate) => {
//     const time_now              = moment()
//     const differences_seconds   = time_now.diff(ReceiveDate, 'second')
//     const differences_text      = ValueFormatter(differences_seconds)
//     const responsetime = differences_text
//     const result = setInterval(responsetime, 1000);
//     console.log('result ResponseTimeUnread', result)
//     return result
// }

export default App;