const path = require('path');
const WebpackBundleAnalyzer = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
const CompressionPlugin = require("compression-webpack-plugin");

module.exports = {
    output: {
        path: path.join(__dirname, '/dist'),
        filename: '[name].js'
    },
    devServer: {
        port: 3010,
        watchContentBase: true
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test:  /\.(jpg|png|gif|jpeg|woff|woff2|eot|ttf|svg)$/,
                use: {
                    loader: 'url-loader'
                }
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    optimization: {
		splitChunks: {
			cacheGroups: {
				commons: {
					test: /[\\/]node_modules[\\/]/,
					name: 'vendors',
					chunks: 'all'
				}
			}
		}
	},
    plugins: [
        new CompressionPlugin({
            algorithm: "gzip",
            test: /\.js(\?.*)?$/i,
            compressionOptions: { level: 1 },
            threshold: 8192,
            minRatio: 0.8,
            filename: "[path][base].gz",
        })
    ]
}